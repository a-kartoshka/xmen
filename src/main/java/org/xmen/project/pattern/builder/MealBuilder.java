package org.xmen.project.pattern.builder;

public interface MealBuilder {

    void buildDrink();

    void buildMainCourse();

    void buildSide();

    Meal getMeal();
}
