package org.xmen.project.pattern.abstractfactory;

import org.xmen.project.pattern.factory.Animal;
import org.xmen.project.pattern.factory.Cat;
import org.xmen.project.pattern.factory.Dog;

public class MammalFactory extends SpeciesFactory{

    @Override
    public Animal getAnimal(String type) {
        if ("dog".equals(type)){
            return new Dog();
        } else {
            return new Cat();
        }
    }
}
