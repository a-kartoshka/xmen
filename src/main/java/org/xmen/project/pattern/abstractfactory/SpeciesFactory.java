package org.xmen.project.pattern.abstractfactory;

import org.xmen.project.pattern.factory.Animal;

public abstract class SpeciesFactory {
    public abstract Animal getAnimal(String type);
}
