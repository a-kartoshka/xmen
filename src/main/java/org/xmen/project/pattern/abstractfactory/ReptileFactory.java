package org.xmen.project.pattern.abstractfactory;

import org.xmen.project.pattern.factory.Animal;
import org.xmen.project.pattern.factory.Snake;
import org.xmen.project.pattern.factory.Tyrannosaurus;

public class ReptileFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {
        if("snake".equals(type)){
            return new Snake();
    } else{
            return new Tyrannosaurus();
        }
    }
}
