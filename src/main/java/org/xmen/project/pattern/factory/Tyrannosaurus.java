package org.xmen.project.pattern.factory;

public class Tyrannosaurus extends Animal {

    @Override
    public String makeSound(){
        return "Roar";
    }
}
