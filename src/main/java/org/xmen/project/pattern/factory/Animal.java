package org.xmen.project.pattern.factory;

public abstract class Animal {
    public abstract String makeSound();
}
