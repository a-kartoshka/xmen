package org.xmen.project.pattern.factory;

public class Cat extends Animal{
    @Override
    public String makeSound(){
        return "Meow";
    }
}
