package org.xmen.project.pattern.factory;

public class Dog extends Animal {
    @Override
    public String makeSound(){
        return "Woof";
    }
}
