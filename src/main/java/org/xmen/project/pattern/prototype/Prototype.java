package org.xmen.project.pattern.prototype;

public interface Prototype {
    Prototype doClone();
}
