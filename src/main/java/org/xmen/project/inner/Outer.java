package org.xmen.project.inner;

public class Outer {

    private static String textNested = "Nested String!";
    private String text = "I am private!";

    public static class Nested{

        public void printNestedText(){
            System.out.println(textNested);
        }

        public static void printStaticNestedText(){}
    }

    public class Inner{
        private String text = "I am Inner private";

        public void printText(){
            System.out.println(text);
            System.out.println(Outer.this.text);
            System.out.println(Inner.this.text);
        }
        public void printStaticField(){
            System.out.println(textNested);
        }
    }

    public void test(){
        Outer.Nested nested = new Outer.Nested();
        nested.printNestedText();
        Outer.Nested.printStaticNestedText();

        Inner inner = new Inner();
        inner.printText();
        inner.printStaticField();
    }

    public void local(){

        class Local {

            private int x;

            public Local(int x){
                this.x =x;
            }

            public Local(){
                x = 5;
            }

             int getX() {
                return this.x;
            }

            Local getNewLocal(){
                return new Local();
            }

            Local getCurrentLocal(){
                return this;
            }

        }

        Local local = new Local(10);
        System.out.println(local.getX());
        System.out.println(local.getNewLocal().getX());
        System.out.println(local.getCurrentLocal().getX());
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.local();

    }

    public void doIt(){
        System.out.println("OuterClass doIt()");
    }
}
