package org.xmen.project.exceptions;

public class CustomerService {

    public static Customer findByName(String name) throws NameNotFoundException{

        if ("".equals(name)){
            throw new NameNotFoundException(678,"Name is empty");
        }

        return new Customer(name);
    }
}
