package org.xmen.project.maps;

import java.util.Comparator;

public class AutoComporator {
    public static  Comparator<Auto> IdComparator = new Comparator<Auto>() {
        @Override
        public int compare(Auto auto1, Auto auto2) {
            return auto1.getId() - auto2.getId();
        }
    };

    public static  Comparator<Auto> tradeMarkComparator = new Comparator<Auto>() {
        @Override
        public int compare(Auto auto1, Auto auto2) {
            return auto1.getModel().compareTo(auto2.getModel());

        }
    };

    public static  Comparator<Auto> modelComparator = new Comparator<Auto>() {
       @Override
        public int compare(Auto o1, Auto o2){
           return o1.getModel().compareTo(o2.getModel());
       }
    };

    public static  Comparator<Auto> yearComparator = new Comparator<Auto>() {
        @Override
        public int compare(Auto o1, Auto o2){
            return o1.getYear() - o2.getYear();
        }
    };
}

