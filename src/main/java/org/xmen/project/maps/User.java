package org.xmen.project.maps;

public class User {
    private String firstName;
    private String lastNAme;
    private int salary;

    public User(String firstName, String lastNAme, int salary) {
        this.firstName = firstName;
        this.lastNAme = lastNAme;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastNAme() {
        return lastNAme;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "\nUser{" +
                "firstName='" + firstName + '\'' +
                ", lastNAme='" + lastNAme + '\'' +
                ", salary=" + salary +
                '}';
    }
}
