package org.xmen.project.maps;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TreeMapExample {
    public static void main(String[] args) {
        Map<String,String> treeMap = new TreeMap<>();
        treeMap.put("Bruce","Willis");
        treeMap.put("Arnold","Schwarz");
        treeMap.put("Jackie","Chan");
        treeMap.put("Sylvester","Stallone");
        treeMap.put("Chuck","Norris");

        for (Map.Entry tree: treeMap.entrySet()) {
            System.out.println(tree.getKey() + " " + tree.getValue());
        }

        //Create a tree map

        TreeMap<String, Double> tradeMarkMap = new TreeMap<>();

        //Put elements tp the map
        tradeMarkMap.put("Zara",8284.37);
        tradeMarkMap.put("Bershka",8284.37);
        tradeMarkMap.put("Cropp",8284.37);
        tradeMarkMap.put("Stradivarius",8284.37);
        tradeMarkMap.put("Pull & Bear",3484.37);

        tradeMarkMap.forEach((key, value) -> {
            System.out.println(key + " = " + value);
            System.out.println();
        });

        System.out.println("-------------------------------------------------------");

        //Get a set of the entries
        Set set = tradeMarkMap.entrySet();

        //Get an iterator
        Iterator i = set.iterator();

        //Display elements
        while (i.hasNext()){
            Map.Entry entryMap = (Map.Entry)i.next();
            System.out.println(entryMap.getKey() + ": ");
            System.out.println(entryMap.getValue());
        }
        System.out.println();

        //Deposit 1000 into Zara's account
        double balance = tradeMarkMap.get("Zara");
        tradeMarkMap.put("Zara",balance + 1000);
        System.out.println("Zara's new balance: " + tradeMarkMap.get("Zara"));

        Map<User, String> userMap = new TreeMap<>(new UserSalaryComparator());

        userMap.put(new User("Nil","Tompson",9826),"My NAme 1");
        userMap.put(new User("Deryl","Logan",45986),"My NAme 2");
        userMap.put(new User("Andriy","Shevchenko",96),"My NAme 3");
        userMap.put(new User("Pedro","Gonzales",9),"My NAme 4");

        System.out.println(userMap.toString());

        Set keySet = userMap.entrySet();
        for (Object user:keySet) {
            System.out.println(user);

        }


//
//        Map<Auto, Integer> autoMap = new TreeMap<>(new AutoComporator());
//        autoMap.put(new Auto(1,"Ford","Mustang",2008),1234);
//        autoMap.put(new Auto(1,"Hummer","A",2014),47345);
//        autoMap.put(new Auto(1,"Fiat","E",2003),8678);
//        autoMap.put(new Auto(1,"Audi","A7",2010),3425);
//        autoMap.put(new Auto(1,"BMW","X5",2009),6546);
//
//        System.out.println(autoMap.toString());
//
//        Set keySet2 = autoMap.entrySet();
//        for (Object auto:keySet) {
//            System.out.println(auto);
//
//        }
    }
}
