package org.xmen.project.maps;

public class Auto {
    private int id;
    private String tradeMark;
    private String model;
    private int year;


    public Auto(int id, String tradeMark, String model, int year) {
        this.id = id;
        this.tradeMark = tradeMark;
        this.model = model;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public String getTradeMark() {
        return tradeMark;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "\nAuto{" +
                "id=" + id +
                ", tradeMark='" + tradeMark + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                '}';
    }
}
