package org.xmen.project.anotations;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
//@Target(ElementType.METHOD)

@Inherited
public @interface MyNewComponent {

    String value();

    String name();
    String age();
    String[] newNames();
    String element() default "elem";
}
