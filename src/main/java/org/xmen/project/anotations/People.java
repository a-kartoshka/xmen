package org.xmen.project.anotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component

public class People implements Serializable, Cloneable {

    private String name;
    private int age;
    public int sum;
    protected int test;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private void test(){}
    private void test2(){}

    @Autowired
    @Deprecated
    protected static void method(String[] params){}

    @Autowired

    @Override
    public String toString() {
        return super.toString();
    }
}
