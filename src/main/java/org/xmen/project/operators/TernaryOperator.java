package org.xmen.project.operators;


public class TernaryOperator {


    /**
     * first to compare
     */
    private Integer first;

    /**
     * second to compare
     */
    private Integer second;

    public TernaryOperator(){

    }

    public TernaryOperator(Integer first, Integer second){
        this.first = first;
        this.second =second;
    }

    public Integer getFirst() {
        return first;
    }

    public void setFirst(Integer first) {
        this.first = first;
    }

    public Integer getSecond() {
        return second;
    }

    public void setSecond(Integer second) {
        this.second = second;
    }

    /**
     * Get Minimal Value
     * @param i
     * @param j
     * @return
     */

    public static int getMinValue(int i, int j){
        return (i < j) ? i : j;
    }


    /**
     * Get Absolute Value
     * @param i
     * @return
     */

    public static int getAbsoluteValue(int i){
        return i < 0 ? -i : i;
    }

    /**
     * invert boolean
     * @param b
     * @return
     */

    public static boolean invertBoolean(boolean b){
        return b ? false : true;
    }

    /**
     * Check if String contains "A"
     * @param str
     */

    public static void containsA(String str){
        String data = str.contains("A") ? "Str contains 'A'" : "Str doesn't contains 'A'";
        System.out.println(data);
    }


    /**
     * ternary method example
     * @param i
     */

    public static void ternaryMethod(Integer i, TernaryOperator ternaryOperator){

        System.out.println((i.equals(ternaryOperator.getFirst()))
        ? "i=5" : ((i.equals(ternaryOperator.getSecond())) ? "i=10" : "i is not equals to 5 or 10"));
    }
}
