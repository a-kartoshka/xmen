package org.xmen.project.strings;


public class User {

    private Integer id;
    private Double price;
    private String userName;

    public User() {
    }

    public User(Integer id, Double price, String userName) {
        this.id = id;
        this.price = price;
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", price=" + price +
                ", userName='" + userName + '\'' +
                '}';
    }
}
