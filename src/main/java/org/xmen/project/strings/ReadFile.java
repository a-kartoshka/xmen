package org.xmen.project.strings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ReadFile {

    public static void readCsv(String path) {

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            String line;

 //               List<User> users = new ArrayList<>();
                 List<Employee> employees = new ArrayList<>();

                while ((line = br.readLine()) != null) {
                    System.out.println(line);

                    StringTokenizer stringTokenizer = new StringTokenizer(line, "|");

                    while (stringTokenizer.hasMoreElements()) {

                        String name = stringTokenizer.nextElement().toString();
                        String surname = stringTokenizer.nextElement().toString();
                        String position = stringTokenizer.nextElement().toString();
                        Integer age = Integer.parseInt(stringTokenizer.nextElement().toString());
                        Double salary = Double.parseDouble(stringTokenizer.nextElement().toString());
                        Double experience = Double.parseDouble(stringTokenizer.nextElement().toString());

//                        Integer id = Integer.parseInt(stringTokenizer.nextElement().toString());
//                        Double price = Double.parseDouble(stringTokenizer.nextElement().toString());
//                        String username = stringTokenizer.nextElement().toString();

//                    StringBuilder sb = new StringBuilder();
//                    sb.append("\nId : " + id);
//                    sb.append("\nPrice : " + price);
//                    sb.append("\nUsername : " + username);
//                    sb.append("\n***********************\n");
//
//                    System.out.println(sb.toString());

//                        users.add(new User(id, price, username));
                        employees.add(new Employee(name, surname, position, age, salary, experience));

                    }
                }

//                for (User user : users) {
//                    System.out.println(user);
//                }

            System.out.println();

            for (Employee employee : employees) {
                System.out.println(employee);
            }

                System.out.println("Done");

            } catch(FileNotFoundException e){
                e.printStackTrace();

            }  catch(IOException e){
                e.printStackTrace();
            }
        }

    }
