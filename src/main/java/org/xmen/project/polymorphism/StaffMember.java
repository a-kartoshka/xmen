package org.xmen.project.polymorphism;

abstract public class StaffMember {

    private String name;
    private String address;
    private String phone;


    public StaffMember(String eName, String eAddress, String ePhone){
        name = eName;
        address = eAddress;
        phone = ePhone;
    }

    @Override
    public String toString() {
        String result = "Name='" + name + "\n";
        result += "Address='" + address + "\n";
        result += "Phone" + phone;
        return result;
    }

    public abstract double pay();
}
