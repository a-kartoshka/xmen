package org.xmen.project.polymorphism;

public class Hourly extends Employee {

    private int hoursWorked;


    /**
     * Sets up an hourly employee with the specified information.
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Hourly(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone, socSecNumber, rate);
        hoursWorked = 0;
    }

    /**
     * Adds the specified number of hours to this employees accumulated hours.
     * @param moreHours
     */
    public void addHours(int moreHours){
        hoursWorked += moreHours;
    }

    /**
     * Computes and returns the pay this hourly employee.
     * @return
     */
    @Override
    public double pay() {
        double payment = super.pay() * hoursWorked;
        hoursWorked = 0;
        return payment;
    }

    /**
     * Returns information about this hourly employee as a string.
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return result;
    }
}
