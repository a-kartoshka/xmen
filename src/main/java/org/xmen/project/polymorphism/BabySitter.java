package org.xmen.project.polymorphism;

public class BabySitter extends Worker implements HourlyPayment, DailyPayment {

    private Boolean knowsOtherLanguages;
    private  int howHours;

    /**
     * Sets up an worker with the specified information.
     * @param id
     * @param name
     * @param age
     * @param oneHourSalary
     * @param knowsOtherLanguages
     */
    public BabySitter(Integer id, String name, Integer age, Double oneHourSalary, Boolean knowsOtherLanguages, int howHours) {
        super(id, name, age, oneHourSalary);
        this.knowsOtherLanguages = knowsOtherLanguages;
        this.howHours = howHours;
    }


    /**
     * Returns information about salary per hour our baby sitter.
     * @return
     */
    @Override
    public double paymentPerHour() {
        return getOneHourSalary();
    }

    /**
     * Returns information about salary per day our baby sitter.
     * @return
     */
    @Override
    public double paymentPerDay() {
       return getOneHourSalary() * getHowHours();
    }


    public int getHowHours() {
        return howHours;
    }

    /**
     * Returns information about an baby sitter as a string.
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nknowsOtherLanguages= " + knowsOtherLanguages;
        result += "\nhowHoursWork= " + howHours;
        result += "\nsalary per day= " + paymentPerDay() + "\n-----------------------";
        return result;
    }
}
