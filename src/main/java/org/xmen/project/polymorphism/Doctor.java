package org.xmen.project.polymorphism;

public class Doctor extends Worker implements ProjectPayment, Premium, MonthlyPayment {

    private double experience;
    private int workDaysMonth;
    private double bonus;
    private double costProject;

    /**
     * Sets up an worker with the specified information.
     * @param id
     * @param name
     * @param age
     * @param oneHourSalary
     * @param experience
     */
    public Doctor(Integer id, String name, Integer age, Double oneHourSalary, double experience,
                  int workDaysMonth, double bonus, double costProject) {
        super(id, name, age, oneHourSalary);
        this.experience = experience;
        this.workDaysMonth = workDaysMonth;
        this.bonus = bonus;
        this.costProject = costProject;
    }

    /**
     * Returns information about salary per month our doctor.
     * @return
     */
    @Override
    public double paymentPerMonth() {
     return getOneHourSalary() * getWorkDaysMonth();
    }

    /**
     * Returns information about premium our doctor.
     * @return
     */
    @Override
    public double bonus() {
     return getBonus();
    }

    /**
     * Returns information about salary for project our doctor.
     * @return
     */
    @Override
    public double paymentForProject() {
     return  getCostProject();
    }

    public int getWorkDaysMonth() {
        return workDaysMonth;
    }

    public double getBonus() {
        return bonus;
    }

    public double getCostProject() {
        return costProject;
    }

    /**
     * Returns information about an doctor as a string.
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nexperience= " + experience;
        result += "\npremium= " + bonus();
        result += "\nsalary for project= " + paymentForProject();
        result += "\nsalary per month= " + paymentPerMonth() + "\n-----------------------";
        return result;
    }
}
