package org.xmen.project.polymorphism;

public interface HourlyPayment {

    double paymentPerHour();
}
