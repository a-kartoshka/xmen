package org.xmen.project.polymorphism;


abstract public class Worker {

     private int id;
     private String name;
     private int age;
     private double oneHourSalary;



     public Worker(int id, String name, int age, double oneHourSalary) {
         this.id = id;
         this.name = name;
         this.age = age;
         this.oneHourSalary = oneHourSalary;
     }

     public double getOneHourSalary() {
         return oneHourSalary;
     }

     public int getId() {
         return id;
     }

     public void setId(int id) {
         this.id = id;
     }

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public int getAge() {
         return age;
     }

     public void setAge(int age) {
         this.age = age;
     }

     public void setOneHourSalary(double oneHourSalary) {
         this.oneHourSalary = oneHourSalary;
     }


    /**
      *  Returns information about an workers as a string.
      * @return
      */
     @Override
     public String toString() {

         String result = "\n" + "ID: " + id + "\n" ;
                result += "name= " + name + "\n";
                 result += "age= " + age + "\n";
                 result += "oneHourSalary= " + oneHourSalary ;
                 return result;
     }
 }
