package org.xmen.project.polymorphism;

public interface ProjectPayment {

    double paymentForProject();
}
