package org.xmen.project.polymorphism;

public class Executive extends Employee{

    private double bonus;

    /**
     * Sets up an executive with the specified information.
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Executive(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super (eName, eAddress, ePhone, socSecNumber, rate);
        // bonus has yet to be awarded
        bonus = 0;
    }

    /**
     * Awards the specified bonus to this executive.
     * @param execBonus
     */
    public void awardBonus(double execBonus){
        bonus = execBonus;
    }

    /**
     * Computes and returns the pay for an executive, which is the regular employee payment plus a one-time bonus.
     * @return
     */
    @Override
    public double pay() {
        double payment = super.pay() + bonus;
        bonus = 0;
        return payment;
    }
}
