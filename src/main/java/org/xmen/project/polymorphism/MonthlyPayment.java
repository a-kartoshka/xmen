package org.xmen.project.polymorphism;

public interface MonthlyPayment {

    double paymentPerMonth();
}
