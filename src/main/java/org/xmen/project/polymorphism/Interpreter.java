package org.xmen.project.polymorphism;

public class Interpreter extends Worker implements ProjectPayment, MonthlyPayment, DailyPayment, HourlyPayment {

    private int numberOfLanguages;
    private  int howHours;
    private int workDaysMonth;
    private int costProject;

    /**
     * Sets up an worker with the specified information.
     * @param id
     * @param name
     * @param age
     * @param oneHourSalary
     * @param numberOfLanguages
     */
    public Interpreter(int id, String name, int age, double oneHourSalary, int numberOfLanguages, int howHours, int workDaysMonth, int costProject) {
        super(id, name, age, oneHourSalary);
        this.numberOfLanguages = numberOfLanguages;
        this.howHours = howHours;
        this.workDaysMonth = workDaysMonth;
        this.costProject = costProject;
    }

    /**
     * Returns information about salary per day our interpreter.
     * @return
     */
    @Override
    public double paymentPerDay() {
   return getOneHourSalary() * getHowHours();
    }

    /**
     * Returns information about salary per hour our interpreter.
     * @return
     */
    @Override
    public double paymentPerHour() {
     return getOneHourSalary();
    }

    /**
     * Returns information about salary per month our interpreter.
     * @return
     */
    @Override
    public double paymentPerMonth() {
     return getOneHourSalary() * getWorkDaysMonth();
    }

    /**
     * Returns information about salary for project our interpreter.
     * @return
     */
    @Override
    public double paymentForProject() {
         return getCostProject();
    }

    public int getHowHours() {
        return howHours;
    }

    public int getWorkDaysMonth() {
        return workDaysMonth;
    }

    public int getCostProject() {
        return costProject;
    }

    /**
     * Returns information about an interpreter as a string.
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nnumberOfLanguages= " + numberOfLanguages;
        result += "\nsalary for project= " + paymentForProject();
        result += "\nsalary per month= " + paymentPerMonth();
        result += "\nsalary per hour= " + paymentPerHour();
        result += "\nsalary per day= " + paymentPerDay() + "\n-----------------------";

        return result;
    }
}
