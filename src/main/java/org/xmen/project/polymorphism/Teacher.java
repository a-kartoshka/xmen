package org.xmen.project.polymorphism;

public class Teacher extends Worker implements MonthlyPayment, Premium {

    private String category;
    private int workDay;
    private double premiumBonus;

    /**
     * Sets up an worker with the specified information.
     * @param id
     * @param name
     * @param age
     * @param oneHourSalary
     * @param category
     */
    public Teacher(int id, String name, int age, double oneHourSalary, String category, int workDay, double premiumBonus) {
        super(id, name, age, oneHourSalary);
        this.category = category;
        this.workDay = workDay;
        this.premiumBonus = premiumBonus;
    }

    /**
     * Returns information about salary per month our teacher.
     * @return
     */
    @Override
    public double paymentPerMonth() {
        return getOneHourSalary() * getWorkDay();
    }

    /**
     * Returns information about premium our teacher.
     * @return
     */
    @Override
    public double bonus() {
     return getPremiumBonus();
    }

    public int getWorkDay() {
        return workDay;
    }

    public double getPremiumBonus() {
        return premiumBonus;
    }

    /**
     * Returns information about an teacher as a string.
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
               result += "\ncategory= " + category;
               result += "\npremium= " + bonus();
               result += "\nsalary per month= " + paymentPerMonth() + "\n-----------------------";
               return result;
    }
}
