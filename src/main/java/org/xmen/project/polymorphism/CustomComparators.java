package org.xmen.project.polymorphism;

import java.util.Comparator;

public class CustomComparators {

    public static  Comparator<Worker> ageComparator = new Comparator<Worker>() {
        @Override
        public int compare(Worker o1, Worker o2){return o1.getAge() - o2.getAge();}
    };

    public static  Comparator<Worker> nameComparator = new Comparator<Worker>() {
        @Override
        public int compare(Worker o1, Worker o2){return o1.getName().compareTo(o2.getName());}
    };

    public static  Comparator<Worker> salaryPerHourComparator = new Comparator<Worker>() {
        @Override
        public int compare(Worker o1, Worker o2){return (int) (o1.getOneHourSalary() - o2.getOneHourSalary());}
    };

    public static  Comparator<Worker> idComparator = new Comparator<Worker>() {
        @Override
        public int compare(Worker o1, Worker o2){return (o1.getId() - o2.getId());}
    };

}
