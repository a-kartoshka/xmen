package org.xmen.project.polymorphism;

public interface Premium {

    double bonus();
}
