package org.xmen.project.polymorphism;

public class User {

    private String firstName;
    private String lastNAme;
    private String loginId;
    private String password;
    private String email;
    private String registered;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastNAme() {
        return lastNAme;
    }

    public void setLastNAme(String lastNAme) {
        this.lastNAme = lastNAme;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

//    public void setLoginId(String loginId) {
//        if (loginId.matches("^[a-zA-Z0-9]*$")) {
//            this.loginId = loginId;
//        } else {
//            System.err.println("Error!");
//        }
//    }

    public String getPassword() {
        return password;
    }

//    public void setPassword(String password) {
//        if(password.length() < 6) {
//            System.err.println("Password is too short");
//        }
//        else {
//            this.password = password;
//        }
//    }

    public void setPassword(String password){
        if (password == null || password.length() < 6){
            System.out.println("Password is too short");
        } else {
            this.password = password;
        }
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }
}
