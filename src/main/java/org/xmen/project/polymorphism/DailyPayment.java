package org.xmen.project.polymorphism;

public interface DailyPayment {

    double paymentPerDay();
}
