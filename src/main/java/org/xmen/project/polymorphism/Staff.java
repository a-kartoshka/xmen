package org.xmen.project.polymorphism;

import java.util.ArrayList;

public class Staff {

    ArrayList<StaffMember> staffList;

    /**
     * Sets up the list of staff members.
     */
    public Staff () {
        staffList = new ArrayList<>();
        staffList.add(new Executive("Sam","123 Main Line","555-0469","123-45-6789",2423.07));
        staffList.add(new Employee("Carla","584 Off Line","786-8632","657-56-4725",1246.15));
        staffList.add(new Employee("Woody","234 Off Rocker","123-5340","234-92-8463",1169.27));
        staffList.add(new Hourly("Diane","204 Fifth Ave","536-9874","943-43-8734",10.55));
        staffList.add(new Volunteer("Norm","129 Suds Blvd","654-0923"));
        staffList.add(new Volunteer("Cliff","496 Duds Lane","765-5663"));

//        ((Executive)staffList.get(0)).awardBonus(500.00);
//        ((Hourly)staffList.get(3)).addHours(40);

        for (StaffMember sm: staffList) {
            if (sm instanceof Executive)
                ((Executive) sm).awardBonus(500.00);
            else if (sm instanceof Hourly)
                ((Hourly)sm).addHours(40);
        }
    }

    /**
     * Pays all staff members.
     */
    public void payday () {
        double amount;
        for (int count = 0; count < staffList.size() ; count++) {
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();

            if (amount == 0.0 ){
                System.out.println("Thanks!");
            } else {
                System.out.println("Paid: " + amount);
            }
            System.out.println("--------------------------");

        }
    }
}
