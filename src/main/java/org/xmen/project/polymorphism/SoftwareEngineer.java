package org.xmen.project.polymorphism;

public class SoftwareEngineer extends Worker implements ProjectPayment, HourlyPayment, MonthlyPayment {

    private boolean fullStack;
    private  double costProject;
    private  int workDaysMounth;

    /**
     * Sets up an worker with the specified information.
     * @param id
     * @param name
     * @param age
     * @param oneHourSalary
     * @param fullStack
     */
    public SoftwareEngineer(int id, String name, int age, double oneHourSalary, boolean fullStack, double costProject, int workDaysMounth) {
        super(id, name, age, oneHourSalary);
        this.fullStack = fullStack;
        this.costProject = costProject;
        this.workDaysMounth = workDaysMounth;

    }

    /**
     * Returns information about salary per hour our dear software engineer.
     * @return
     */
    @Override
    public double paymentPerHour() {
      return getOneHourSalary();
    }

    /**
     * Returns information about salary for project our dear software engineer.
     * @return
     */
    @Override
    public double paymentForProject() {
        return getCostProject();
    }

    /**
     * Returns information about salary per month our dear software engineer.
     * @return
     */
    @Override
    public double paymentPerMonth() {
       return getOneHourSalary() * getWorkDaysMounth();
    }

    public double getCostProject() {
        return costProject;
    }

    public int getWorkDaysMounth() {
        return workDaysMounth;
    }

    /**
     * Returns information about an software engineer as a string.
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nfull stack= " + fullStack;
        result += "\nsalary for project= " + paymentForProject();
        result += "\nsalary per month= " + paymentPerMonth();
        result += "\nsalary per hour= " + paymentPerHour() + "\n-----------------------";
        return result;
    }
}
