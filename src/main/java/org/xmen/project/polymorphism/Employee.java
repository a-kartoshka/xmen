package org.xmen.project.polymorphism;

public class Employee extends StaffMember{

    private String socialSecurityNumber;
    private double payRate;

    /**
     * Sets up an executive with the specified information.
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Employee(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone);
        socialSecurityNumber = socSecNumber;
        payRate = rate;
    }

    /**
     * Returns information about an employee as a string.
     * @return
     */
    @Override
    public String toString() {
        String result = super.toString();
        result += "\nSocial Security Number: " + socialSecurityNumber;
        return result;
    }

    @Override
    public double pay() {
        return payRate;
    }
}
