package org.xmen.project.generics;

import java.util.Collection;
import java.util.Collections;

public class GenericSmpl <T>{

    void test(T t){
        System.out.println(t.toString());
    }

    public  <T> T  addAndReturn(T element, Collection<T> collection){
        collection.add(element);
        return element;
    }


}
