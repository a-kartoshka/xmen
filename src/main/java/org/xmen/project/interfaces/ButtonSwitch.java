package org.xmen.project.interfaces;

public interface ButtonSwitch {

    void switchON();
    void switchOFF();
}
