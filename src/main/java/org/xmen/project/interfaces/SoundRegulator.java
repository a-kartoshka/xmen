package org.xmen.project.interfaces;

public interface SoundRegulator {

    void up();
    void down();
}
