package org.xmen.project.functional;

@FunctionalInterface
public interface ComplexFunctionalInterface extends SimpleFuncInterface{

    int x = 5;


    static void doSomeWork(){
        System.out.println("Doing some work in interface impl...");
    }

    @Override
    default void doSomeOtherWork() {
        System.out.println("Doing some other work in interface impl...");
    }

    @Override
    void doWork();

    @Override
    String toString();

    @Override
    boolean equals(Object o);
}
