package org.xmen.project.enums;

public class PlanetsEnums {

    public enum Planets{

        PLANET_MERCURY(2,"Mercury") {
            @Override
            public int distance() {
                return Planets.PLANET_MERCURY.distance;
            }

            @Override
            public String names() {
                return Planets.PLANET_MERCURY.name;
            }
        },

        PLANET_VENUS(3,"Venus") {
            @Override
            public int distance() {
                return Planets.PLANET_VENUS.distance;
            }

            @Override
            public String names() {
                return Planets.PLANET_VENUS.name;
            }
        },

        PLANET_EARTH(4,"Earth") {
            @Override
            public int distance() {
                return Planets.PLANET_EARTH.distance;
            }

            @Override
            public String names() {
                return Planets.PLANET_EARTH.name;
            }
        },

        PLANET_MARS(5,"Mars") {
            @Override
            public int distance() {
                return Planets.PLANET_MARS.distance;
            }

            @Override
            public String names() {
                return Planets.PLANET_MARS.name;
            }
        },

        PLANET_JUPITER(6,"Jupiter") {
            @Override
            public int distance() {
                return Planets.PLANET_JUPITER.distance;
            }

            @Override
            public String names() {
                return Planets.PLANET_JUPITER.name;
            }
        };

        private final int distance;
        private final String name;


        Planets(int distance, String name) {
            this.distance = distance;
            this.name = name;
        }


        public abstract int distance();
        public abstract String names();
    }
}
