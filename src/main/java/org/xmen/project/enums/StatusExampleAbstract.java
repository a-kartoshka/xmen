package org.xmen.project.enums;

public class StatusExampleAbstract {


    public enum Status {

        STATUS_OPEN(2) {
            public String description() {
                return "open";
            }
        },
        STATUS_STARTED(3) {
            public String description() {
                return "stardet";
            }
        },
        STATUS_ONHOLD(4) {
            public String description() {
                return "onhold";
            }
        },
        STATUS_COMPLETED(5) {
            public String description() {
                return "completed";
            }
        },
        STATUS_CLOSED(6) {
            public String description() {
                return "closed";
            }
        };

        int x;

        Status(int x) {
            this.x = x;
        }

        public abstract String description();

        public static void data(String value) {
            for (Status stat : Status.values()) {
                if (stat.name().equalsIgnoreCase(value)) {
                    System.out.println(stat.description());
                }

            }
        }

    }

    public static void main(String[] args) {
        Status.data("Status_onHold");
    }
}

