package org.xmen.project.loopandarrays;

public class Matrix {


    public static Double[][] multiplicar(Double[][] first, Double[][] second){

        int firstColums = first.length;
        int firstRows = first[0].length;
        int secondColums = second.length;
        int secondRows = second[0].length;

        if (firstRows != secondColums){
            throw  new IllegalArgumentException("First Rows: " + firstRows + " did not match Second Columns " + secondColums + ".");

        }

        Double[][] result = new Double[firstRows][secondColums];
        for (int i = 0; i < firstColums; i++) {
            for (int j = 0; j < secondColums; j++) {
                result[i][j] = 0.00000;

            }

        }

        for (int i = 0; i <  firstColums; i++) {//firstRow
            for (int j = 0; j < secondColums; j++) { //secondColumn
                for (int k = 0; k <result.length ; k++) {//resultColum
                    result[i][j] += first[i][k] * second[k][j];

                }

            }
        }

        return result;

    }
}
