package org.xmen.project.loopandarrays;


public class SequentialSearch {

 public static void array(int[] myArray, int val){

     boolean found = false;

     for (int x : myArray) {
         if (x == val) {
             found = true;
             break;
         }
     }

     if (found)
         System.out.println("Значение найдено! Это-" + val);
     else
         System.out.println("Такое значение-" + val + " не найдено!" );
 }

}
