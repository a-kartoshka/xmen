package org.xmen.project.loopandarrays;

import java.util.Scanner;

public class WhileLoops {

    public static void main(String[] args) {

        System.out.println("If you want to start,please, enter key - 'y' or 'Y' ");

        System.out.println();

        System.out.println("If you want to exit,please, enter any other key");

        Scanner scanner = new Scanner(System.in);
        selectChoice(scanner.next());

    }

    public static void selectChoice(String str) {

            while (str.equals("y") || str.equals("Y")) {

                System.out.println("What is the command keyword to exit a loop in Java?");

                System.out.println("a.quit");
                System.out.println("b.continue");
                System.out.println("c.break");
                System.out.println("d.exit");

                System.out.println();
                System.out.println("If you want to exit,please,enter key - 'Q' or 'q'");

                System.out.println();
                System.out.println("Enter your choice:");
                Scanner enter = new Scanner(System.in);
                String choice = enter.next();


                if (choice.equals("c")) {
                    System.out.println("Congratulation!");
                    break;

                } else if (choice.equals("q") || choice.equals("Q")) {
                    System.out.println("Exiting...!");
                    break;

                } else
                    System.out.println("Incorrect!");
                System.out.println("Again? press any key to continue:");

                Scanner scanner1 = new Scanner(System.in);
                selectChoice(scanner1.next());

            }
        }
    }
