package org.xmen.project.loopandarrays;

public class BubbleSort {

    public static void sort(int [] myArray){

        for (int i = 0; i < myArray.length ; i++) {
            int right = myArray[i];

            for (int j = i - 1; j >= 0; j--) {
                int left = myArray[j];

                if (right < left){
                    myArray[j + 1] = left;
                    myArray[j] = right;
                } else
                    break;

            }

        }
        for (Integer array : myArray) {
            System.out.print("{" + array + "} " );

        }
    }

}
