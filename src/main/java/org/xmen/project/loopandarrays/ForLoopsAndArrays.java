package org.xmen.project.loopandarrays;

import java.lang.reflect.Array;

import java.util.Arrays;

public class ForLoopsAndArrays {

    public static boolean equalsOfTwoArrays(int[] myArray1, int[] myArray2) {

        boolean equalOrNot = true;

        if ((myArray1 != null && myArray2 != null) && myArray1.length == myArray2.length)  {

            for (int i = 0; i < myArray1.length; i++) {

                if (myArray1[i] != myArray2[i])
                    equalOrNot = false;
            }

        } else if (myArray1 == null && myArray2 == null) {

        } else

            equalOrNot = false;

        return equalOrNot;
    }


    public static void uniqueArray(int[] myArray){

        int arraySize = myArray.length;

        System.out.println("Original Array : ");

        for (int i = 0; i < arraySize ; i++)

            System.out.println(myArray[i] + "\t");

        //Comparing each element with all other elements

        for (int i = 0; i < arraySize; i++) {

            for (int j = i + 1; j < arraySize; j++) {

                //If any two elements are found equal
                if (myArray[i] == myArray[j]){

                    //Replace duplicate element with last unique element
                    myArray[j] = myArray[arraySize - 1];

                    arraySize--;

                    j--;
                }

            }

        }


        //Copying only unique elements of my_array into array1
        int[] array1 = Arrays.copyOf(myArray, arraySize);

        //Printing arrayWithoutOuplicates
        System.out.println();

        System.out.println("Array with unique values : ");

        for (int i = 0; i < array1.length ; i++)

            System.out.println(array1[i] + "\t");


    }

}


