package org.xmen.project.loopandarrays;

public class GnomeSort {

    public static void gnome(int [] array) {

        int first = 1;

        while (first < array.length){

            if(first == 0 || array[first - 1] <= array[first])
                first++;
            else {
                int temp = array[first];
                array[first] = array[first - 1];
                array[first - 1] = temp;
                first--;
            }
        }

        for (Integer arr : array) {
            System.out.print("{" + arr + "} ");
        }

    }
}
