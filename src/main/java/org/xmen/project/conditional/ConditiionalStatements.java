package org.xmen.project.conditional;

public class ConditiionalStatements {

    /**
     * Checkn if  value is positive or negative
     * @param x
     */

    public static void positiveOrNegative(Integer x){
        if (x > 0){
            System.out.println("Number " + x + " is positive");
        } else if (x < 0){
            System.out.println("Number " + x + " is negative");
        } else {
            System.out.println("Number " + x + " is zero");
        }
    }
}
