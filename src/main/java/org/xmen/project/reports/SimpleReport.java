package org.xmen.project.reports;

import net.sf.dynamicreports.adhoc.AdhocManager;
import net.sf.dynamicreports.adhoc.configuration.AdhocColumn;
import net.sf.dynamicreports.adhoc.configuration.AdhocConfiguration;
import net.sf.dynamicreports.adhoc.configuration.AdhocReport;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Date;

public class SimpleReport {

    public SimpleReport(){
        build();
    }


    private void build(){

        AdhocConfiguration configuration = new AdhocConfiguration();
        AdhocReport report = new AdhocReport();
        configuration.setReport(report);

        AdhocColumn colum = new AdhocColumn();
        colum.setName("item");
        report.addColumn(colum);

        colum = new AdhocColumn();
        colum.setName("orderdate");
        report.addColumn(colum);

        colum = new AdhocColumn();
        colum.setName("quantity");
        report.addColumn(colum);

        try {
            AdhocManager.saveConfiguration(configuration, new FileOutputStream("/home/tony/IdeaProjects/xmen/reports_configuration.xml"));
            AdhocConfiguration loadConfiguration = AdhocManager.loadConfiguration(new FileInputStream("/home/tony/IdeaProjects/xmen/reports_configuration.xml"));

            JasperReportBuilder reportBuilder = AdhocManager.createReport(configuration.getReport());
            reportBuilder.setDataSource(createDataSource());
            reportBuilder.toPdf(new FileOutputStream(new File("SimpleReportPdf")));
        }catch (DRException e){
            e.printStackTrace();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }



    private JRDataSource createDataSource(){
        DRDataSource dataSource = new DRDataSource("item", "orderdate", "quantity","unitprice");
        for (int i = 0; i < 20 ; i++) {
            dataSource.add("Book",new Date(),(int) (Math.random()* 10) + 1, new BigDecimal(Math.random()*100+1));

        }
        return dataSource;
    }
    public static void main(String[] args){
        new SimpleReport();
    }
}
