package org.xmen.project.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

public class TestMainImage {

    public static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) throws Exception {

        String mainLink = "http://maven.apache.org/";
        List<String> links = new ArrayList<>();
        List<String> links1 = new ArrayList<>();

        Integer threadsQuantity = 10;

        Document document = Jsoup.connect(mainLink).get();
        Elements linkTags = document.select("a[href]");
        Elements images = document.getElementsByTag("img");

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadsQuantity);

        String link;
        for (Element element: linkTags) {
            link = element.attr("href");
            if (link.startsWith("http://maven.apache.org/")){
                links.add(link);
            }
        }

        String image;
        for (Element element: images) {
            image = element.attr("img");
            if (image.startsWith("http://maven.apache.org/")){
                links1.add(image);
            }
        }



        for (int i = 0; i < threadsQuantity; i++) {
            GetImages threadsController = new GetImages(links.get(i));
            executor.execute(threadsController);
        }

        for (int i = 0; i < threadsQuantity; i++) {
            GetImages threadsController = new GetImages(links1.get(i));
            executor.execute(threadsController);
        }
        executor.shutdown();
    }
}
