package org.xmen.project.inheritance;

public class Square extends Figure {

    private double side;
    private double a;

    public Square(double a) {
        this.a = a;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double getArea() {
        double s  = a * a;
        System.out.println("Площадь квадрата " + s + " при стороне a-" + a);
        return s;
    }
}
