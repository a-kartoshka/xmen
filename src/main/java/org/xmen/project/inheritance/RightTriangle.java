package org.xmen.project.inheritance;

public class RightTriangle extends Triangle{
    private double a;
    private double b;

    public RightTriangle(double a, double b) {
        this.a = a;
        this.b = b;
    }


    @Override
    public double getArea() {

        double s = 0.5 * a * b;

        System.out.println("Площадь прямоугольного треугольника " + s + " при сторонах a-" + a + " и b-" + b);

        return s;

    }
}
