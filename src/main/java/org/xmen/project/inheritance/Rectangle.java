package org.xmen.project.inheritance;

public class Rectangle extends Square {

    private double a ;
    private double b ;

    public Rectangle(double a, double b) {
        super(a);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getArea() {
        double s = a*b;
        System.out.println("Площадь прямоугольника " + s + " при сторонах a-" + a +" и b-" + b);
        return s;
    }
}
