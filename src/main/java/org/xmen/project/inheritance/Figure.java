package org.xmen.project.inheritance;

public abstract class Figure {

    abstract double getArea();

}
