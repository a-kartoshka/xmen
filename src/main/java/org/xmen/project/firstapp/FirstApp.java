package org.xmen.project.firstapp;

import static java.lang.System.*;

public class FirstApp {

    public FirstApp(){}

    /**
     * Our version for System.out.print
     * @param t
     * @param <T>
     */

    public static <T> void print(T t) {
        out.print(t);
    }

    /**
     * Our version for System.out.printl
     * @param object
     */
    public static void println(Object object){
        out.print(object);
    }

    /**
     * Our version for System.out.println
     */

    public static void println() {
        out.println();
    }


}
