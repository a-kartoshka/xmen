package org.xmen.project.firstapp;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.xmen.project.firstapp.FirstApp.*;

public class FirstAppTest {

    Integer x = 2017;

    /**
     * Testing our version for System.out.print and println methods
     */

    @Test
    public void firstAppTest(){
        print("Hello World");
        println();
        println(x);

//        List<Integer> integers = new ArrayList<>();
//        integers.add(2);
//        integers.add(10);
//        integers.add(12);
//
//        for (Integer i:integers){
//            System.out.println(i);
//        }
//
//        integers.forEach(System.out::println);
    }
}
