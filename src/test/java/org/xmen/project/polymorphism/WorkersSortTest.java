package org.xmen.project.polymorphism;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static junit.framework.TestCase.assertEquals;

public class WorkersSortTest {

    @Test
    public  void sort(){


        Worker babySitter1 = new BabySitter(23,"Dasha",24,1.50,true,6);
        Worker babySitter2 = new BabySitter(234,"Patrick",19,0.50,false,8);
        Worker doctor1 = new Doctor(432,"Samanta",49,60.0,15.7,17,2000,546);
        Worker doctor2 = new Doctor(24,"Richard",58,40.0,7.9,10,12000,64678);
        Worker doctor3 = new Doctor(3,"Dodick",39,45.0,2.5,12,1000,4654);
        Worker interpreter1 = new Interpreter(34,"Yana",29,17.0,5,7,26,46554);
        Worker interpreter2 = new Interpreter(35,"Pol",23,10.0,2,8,15,5466);
        Worker interpreter3 = new Interpreter(2,"Liza",21,12.0,3,6,20,6546);
        Worker softwareEngineer1 = new SoftwareEngineer(9,"Nanya",20,100.0,true,37876,25);
        Worker softwareEngineer2 = new SoftwareEngineer(17,"Bob",25,30.0,false,4562,19);
        Worker teacher1 = new Teacher(11,"Galya",40,1.50,"hight",20,3456);

        ArrayList<Worker> workersList = new  ArrayList<>();
        workersList.add(babySitter1);
        workersList.add(babySitter2);

        workersList.add(doctor1);
        workersList.add(doctor2);
        workersList.add(doctor3);

        workersList.add(interpreter1);
        workersList.add(interpreter2);
        workersList.add(interpreter3);

        workersList.add(softwareEngineer1);
        workersList.add(softwareEngineer2);

        workersList.add(teacher1);
        

        System.out.println("Unsorted List : " + workersList);


        Collections.sort(workersList, CustomComparators.ageComparator);
        assertEquals(babySitter2, workersList.get(0));
        assertEquals(doctor2, workersList.get(10));

        System.out.println();
        System.out.println("Sorting by age order : " + workersList);


        Collections.sort(workersList, CustomComparators.nameComparator);
        assertEquals(softwareEngineer2, workersList.get(0));
        assertEquals(interpreter1, workersList.get(10));

        System.out.println();
        System.out.println("Sorting by name order : " + workersList);


        Collections.sort(workersList, CustomComparators.salaryPerHourComparator);
        assertEquals(babySitter2, workersList.get(0));
        assertEquals(softwareEngineer1, workersList.get(10));

        System.out.println();
        System.out.println("Sorting by salary per hour order : " + workersList);


        Collections.sort(workersList, CustomComparators.idComparator);
        assertEquals(interpreter3, workersList.get(0));
        assertEquals(doctor1, workersList.get(10));

        System.out.println();
        System.out.println("Sorting by ID order : " + workersList);


    }
}
