package org.xmen.project.conditional;

import org.junit.Test;

import static org.xmen.project.conditional.ConditiionalStatements.*;

public class ConditionalStatementsTest {

    @Test
    public void posOrNegTest(){
        Integer x = -5;
        Integer y = 10;
        Integer q = 0;
        positiveOrNegative(x);
        positiveOrNegative(y);
        positiveOrNegative(q);

    }
}
