package org.xmen.project.loopandarrays;

import org.junit.Test;

import static org.xmen.project.loopandarrays.Matrix.multiplicar;

public class MatrixTest {

    @Test
    public void matrixMultiplication(){

        Double[][] first = {{4.00, 3.00, 5.67},
                            {2.00, 1.00, 5.67},
                            {5.43, 4.12, 5.67}};
        Double[][] second = {{-0.500, 1.500, 5.67},
                             {2.00, -2.0000, 5.67},
                             {3.23, 6.15, 5.67}};

        Double[][] result = multiplicar(first, second);

        for (int i = 0; i < first.length ; i++) {
            for (int j = 0; j < first.length ; j++) {
                System.out.println(result[i][j] + " ");
                System.out.println();

            }

        }
    }
}
