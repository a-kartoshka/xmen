package org.xmen.project.loopandarrays;

import org.junit.Test;

import static org.xmen.project.loopandarrays.ForLoopsAndArrays.uniqueArray;

public class ForLoopsAndArraysTest {

    @Test
    public void checkingEqualityOfTwoArrays(){
        int[] array1 = null;
        int[] array2 = {4,7,5,6,6};
        int[] array3 = {4,7,5,6,6};
        int[] array4 = {2,4,2,6,8};
        int[] array5 = null;
        int[] array6 = new int[0];
        int[] array7 = new int[0];
        if (ForLoopsAndArrays.equalsOfTwoArrays(array1,array5)){
            System.out.println("Two arrays are equal.");
        }else {
            System.out.println("Two arrays are not equal.");
        }
    }
    @Test
    public void uniqueElements(){
        uniqueArray(new int[] {0, 3, -2, 4, 3, 2 } );
        System.out.println();
        System.out.println();
        uniqueArray(new int[] {10, 22, 10, 20, 11, 22});
    }
}
