package org.xmen.project.loopandarrays;

import org.junit.Test;

import static org.xmen.project.loopandarrays.BubbleSort.sort;

public class BubbleSortTest {

    @Test
    public void bubble(){
        sort(new int[] {2,1,345,73,4247,8});
        System.out.println();
        sort(new int[] {143,25,1,73,-47,0});
    }
}
