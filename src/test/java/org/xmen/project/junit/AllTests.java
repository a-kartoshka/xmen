package org.xmen.project.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DataMethodsTest.class,
        DataMethodsTest2.class})
public class AllTests{}


