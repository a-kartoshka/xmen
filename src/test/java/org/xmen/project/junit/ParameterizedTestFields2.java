package org.xmen.project.junit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.Collection;
@RunWith(Parameterized.class)
public class ParameterizedTestFields2 {

    private int m1;
    private int m2;

    public ParameterizedTestFields2(int m1, int m2){
        this.m1 = m1;
        this.m2 = m2;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        Object[][] data  = new Object[][]{{1,2},{5,3},{121,4}};
        return Arrays.asList(data);
    }

    @Test
    public void testMultiplyException(){
        DataMethods tester = new DataMethods();
        assertEquals("Result",m1 * m2,tester.multiply(m1,m2));
    }
}
