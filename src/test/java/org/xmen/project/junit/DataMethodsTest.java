package org.xmen.project.junit;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DataMethodsTest {

    @Test
    public void testFindMax(){
        assertEquals(4,DataMethods.findMax(new int[]{1,2,3,4,2}));
        assertEquals(-1,DataMethods.findMax(new int[]{-12,-1,-3,-4-4}));

        int test = DataMethods.findMax(new int[]{2,3,1,5});
        assert test == 5;
    }

    @Test
    public void multiplicationOfZeroIntegersShouldReturnZero(){
     //   DataMethods dataMethods = new DataMethods();

        assertEquals("10 x 0 must be 0",1,DataMethods.multiply(10,0));
        assertEquals("10 x 0 must be 0",0,DataMethods.multiply(10,0));
        assertEquals("10 x 0 must be 0",0,DataMethods.multiply(10,0));

    }
}
