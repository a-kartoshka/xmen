package org.xmen.project.junit;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class DataMethodsTest2 {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        System.out.println("before class");
    }
    @Before
    public void setUp() throws Exception{
        System.out.println("before ");
    }
    @Test
    public void testFindMax() throws Exception{
        System.out.println("test case find max");
        assertEquals(4,DataMethods.findMax(new int[]{1,3,4,2}));
        assertEquals(-2,DataMethods.findMax(new int[]{-12,-3,-4,-2}));

    }
    @Test
    public void testCube() {
        System.out.println("test case cube");
        assertEquals(27,DataMethods.cube(3));

    }
    @Test
    public void testReverseWord() {
        System.out.println("test case reverse word");
        assertEquals("ym eman si nahk ",DataMethods.reversWord("my name is khan"));
    }

    @After
    public void tearDown()throws Exception{
        System.out.println("after");
    }

    @AfterClass
    public static void tearDownAfterClass()throws Exception{
        System.out.println("after class");
    }

}
