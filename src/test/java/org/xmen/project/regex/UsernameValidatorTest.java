package org.xmen.project.regex;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UsernameValidatorTest {

    private UsernameValidator usernameValidator;

    @BeforeClass
    public void initData(){
        usernameValidator = new UsernameValidator();
    }

    @DataProvider
    public Object[][] validUsernameProvider() {
        return new Object[][]{ {new String[] { "ale", "alex_2002","alex-2002" ,"alex3-4_good"}}
        };
    }

    @DataProvider
    public Object[][] invalidUsernameProvider() {
        return new Object[][]{{new String[] {"al","al@eksey","alekey123456789_-"}}
        };
    }

    @Test(dataProvider = "validUsernameProvider")
    public void validUsernameTest(String[] username) {

        for(String temp : username){
            boolean valid = usernameValidator.validate(temp);
            System.out.println("Username is valid : " + temp + " , " + valid);
            Assert.assertEquals(true, valid);
        }

    }

    @Test(dataProvider = "invalidUsernameProvider",
            dependsOnMethods="validUsernameTest")
    public void inValidUsernameTest(String[] username) {

        for(String temp : username){
            boolean valid = usernameValidator.validate(temp);
            System.out.println("username is valid : " + temp + " , " + valid);
            Assert.assertEquals(false, valid);
        }

    }
}
