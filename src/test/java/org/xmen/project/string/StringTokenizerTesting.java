package org.xmen.project.string;

import org.junit.Test;
import org.xmen.project.strings.ReadFile;
import org.xmen.project.strings.StringTokenizerUtil;

public class StringTokenizerTesting {

    @Test
    public void tokenize(){

        String str ="Hello World OK";
        String str2 ="Hello, World, OK";

        StringTokenizerUtil.splitBySpace(str);
        StringTokenizerUtil.splitByComma(str2);

    }

    @Test
    public void readCSV(){
       String path = "/home/tony/IdeaProjects/xmen/src/main/resources/employeefile.csv";

       ReadFile.readCsv(path);
    }
}
