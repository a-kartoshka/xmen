package org.xmen.project.interfaces;

import org.junit.Test;

import java.util.*;

public class SortAnArrayTest {

    @Test
    public void sortAnArray(){

        String[] fruits = new String[]{"Pineapple","Apple","Orange","Banana"};

        Arrays.sort(fruits);

        int i = 0;
        for (String temp:fruits) {
            System.out.println(" fruits " + ++i + " : " + temp);
        }

    }

    @Test
    public void sortAnArrayList(){

        List<String> fruits = new ArrayList<>();

        fruits.add("Pineapple");
        fruits.add("Apple");
        fruits.add("Orange");
        fruits.add("Banana");

        Collections.sort(fruits);

        int i = 0;
        for (String temp : fruits) {
            System.out.println("fruits " + ++i + " : " + temp);
        }
    }
}
