package org.xmen.project.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class NewExcel {
    @Test
    public void test(){

        //Create blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();


        //Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet("Employee Info");


        //This data needs to be written(Object[])
        Map<String,Object[]> empinfo = new TreeMap<>();

        empinfo.put("1",new Object[]{"EMP ID","EMP NAME","DESIGNATION"});
        empinfo.put("2",new Object[]{"tp01", "Vovan","Technical Manager"});
        empinfo.put("3",new Object[]{"tp02", "Ivan","Proof Reader"});
        empinfo.put("4",new Object[]{"tp03", "Jack","Technical Writer"});
        empinfo.put("5",new Object[]{"tp04", "Leo","Technical Writer"});
        empinfo.put("6",new Object[]{"tp05", "Oleg","Technical Writer"});

        //Iterate over data and write to sheet
        Set<String> keyId = empinfo.keySet();

        //Create row object
        XSSFRow row;

        int rowid = 0;

        for (String key : keyId) {
            row = spreadsheet.createRow(rowid++);
            Object[] objects = empinfo.get(key);
            int celllid = 0;
            for (Object obj:objects) {
                Cell cell = row.createCell(celllid++);
                cell.setCellValue((String)obj);
            }
        }

        //Write the workbook in file system
        try (FileOutputStream out = new FileOutputStream(new File("Writesheet.xlsx"))){
            workbook.write(out);
            out.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("Writesheet.xlsx written successfully");
    }
}
