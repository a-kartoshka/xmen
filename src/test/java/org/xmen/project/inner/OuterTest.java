package org.xmen.project.inner;

import org.junit.Test;

public class OuterTest {

    public static int x;
    public int y;

    public static int x1;

    static {
        x = 3;
        //y = 10;
    }

    {
        x = 6;
        y = 5;
    }

    public void print() {
        System.out.println(x + x1 + y);
    }

    public static void print2() {
        System.out.println(x + x1);
    }

    @Test
    public void tets() {
        OuterTest outerTest = new OuterTest();
        outerTest.print();
        OuterTest.print2();
    }

    @Test
    public void outerTest() {
        Outer.Nested nested = new Outer.Nested();
        nested.printNestedText();

        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        inner.printText();
        outer.doIt();

        Outer outer1 = new Outer() {

            @Override
            public void doIt() {
                System.out.println("Anonymous class doIt()");
            }
        };

        outer1.doIt();

        Outer outer2 = new Outer() {
            @Override
            public void doIt() {
                super.doIt();
            }
        };

    }

    @Test
    public void cacheTest(){
        String key ="Password";
        String key2 ="Name";
        Integer pass =234324;
        String name ="Ivan";

        Cache cache = new Cache();
        cache.store(key, pass);
        cache.store(key2,name);

        System.out.println(cache.get("Password"));
        System.out.println(cache.getData("Password"));
        System.out.println(cache.get("Name"));

    }
}

